# flutter_app
| Custom Toast |
|-------------------------------|
| <img src="https://github.com/afifPathan/FlutterExperiment/blob/master/Custom%20Toast%20Message.png" height="400" alt="Screenshot"/> |

| Add Item In List on Button Click with Custom Dialouge |
|-------------------------------|
| <img src="https://github.com/afifPathan/FlutterExperiment/blob/master/Add%20Item%20In%20List%20with%20Dialouge%20.png" height="400" alt="Screenshot"/> |

| Custom ListView |
|-------------------------------|
| <img src="https://github.com/afifPathan/FlutterExperiment/blob/master/Custom%20List%20view.png" height="400" alt="Screenshot"/> |

| Single Item Selection In List |
|-------------------------------|
| <img src="https://github.com/afifPathan/FlutterExperiment/blob/master/single%20Item%20Select%20In%20List.png" height="400" alt="Screenshot"/> |

| Multiple Item Selection In List |
|-------------------------------|
| <img src="https://github.com/afifPathan/FlutterExperiment/blob/master/Multiple%20item%20Select%20In%20List.png" height="400" alt="Screenshot"/> |
